'''Test cases for quality-trim'''

import unittest
import os, shutil, subprocess, sys

try:
    from Bio import SeqIO
except ImportError:
    print ("Missing BioPython")
    exit(1)

_sequence = "TGACTTACGACAATGCCCATGATGTTGAGCTCATCGGGCGATGTCGATTCAGGCGCGGTTCATAGTATTCGAGCATTGGCAAAACC"\
            "TCCTTTGCAATCCTCTGATCTGTCAGCAACAGAAAGAAACAGGAGGTTATCAGATGAGGATAAGGGATATCTCGCTTGCAAAGGAA"

testInFileName = 'test.fq'
testInFileNameGZ = '%s.gz' % testInFileName
qualityTrimApp = os.path.dirname(os.path.abspath(__file__)) + '/../bin/quality-trim' 

trimmedFile = "%s-trimmed.fastq" % (os.path.splitext(testInFileName)[0],)
lowQualFile = "%s-lowquality.fastq" % (os.path.splitext(testInFileName)[0],)


'''
Char   !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
       |              |          |    |        |                              |                     |
Ascii 33             44         59   64       73                            104                   126
Phred  0.2...........15.........26...31........41 
'''


class TestQualityTrim(unittest.TestCase):
    '''A super class for test classes that performs testing on quality-trim (c program)'''
    
    currentResult = None
    errorcount = 0
    
    def setUp(self):
        if os.path.exists(self._testMethodName):
            shutil.rmtree(self._testMethodName)
        os.mkdir(self._testMethodName)
        os.chdir(self._testMethodName)
        
        # store error count for later (hack)
        self.errorcount = len(self.currentResult.errors) + len(self.currentResult.failures)
        
    def run(self, result=None):
        self.currentResult = result # remember result for use in tearDown
        unittest.TestCase.run(self, result) # call superclass run method
        
    def tearDown(self):
        os.chdir('..')
        
        # hack to cleanup only when successful
        errors = len(self.currentResult.errors) + len(self.currentResult.failures)
        if errors == self.errorcount:
            shutil.rmtree(self._testMethodName)
        self.errorcount = errors

    def assertFileLen(self, filename, length, msg):
        f = open(filename, 'r')
        self.assertEqual(len(f.readlines()), length, msg)

    def assertSeqEquals(self, filename, seq, msg):
        handle = open(filename, "rU")
        seqfound = False
        for record in SeqIO.parse(handle, "fastq"):
            self.assertEqual(str(record.seq), str(seq), msg)
            seqfound = True
            break
        self.assertTrue(seqfound, "%s, No sequence in file" % msg)
        handle.close()

## test classes ##

class TestAvgQual(TestQualityTrim):
    '''Checks the avg quality functions correctly'''
     
    def test_low_avg_quality14(self):
        writeSeq([('test', _sequence[:60], qual((14,)) * 60)])
        runQualityTrim()
        self.assertFileLen(lowQualFile, 4, "Avg Quality of 14 should fail")
     
    def test_low_avg_quality19p99(self):
        writeSeq([('test', _sequence[:100], (qual((20,)) * 99) + qual((19,)))])
        runQualityTrim()
        self.assertFileLen(lowQualFile, 4, "Avg Quality of 19.99 should fail")
     
    def test_low_avg_quality20(self):
        writeSeq([('test', _sequence[:60], qual((20,)) * 60)])
        runQualityTrim()
        self.assertFileLen(trimmedFile, 4, "Avg Quality of 20 should pass")
         
 
class TestEndTrimming(TestQualityTrim):
    '''Checks that the end of a sequence is trimmed correctly'''
     
    def test_end_trim(self):
        seq = _sequence[:70]
        qualph = [21]*60
        qualph.extend(range(20,10,-1))
        qualstr = qual(qualph)
        writeSeq([('test', seq, qualstr)])
        runQualityTrim()
        self.assertSeqEquals(trimmedFile, _sequence[:66], "End trim should be at base 66 (...%s)" % (_sequence[60:66],))
 
class TestChastityFilter(TestQualityTrim):
    ''''''
     
    def test_chastity_Y(self):
        writeSeq([('M1:59:TEST:6:1101:2266:2233 1:Y:0:GGCGTA', _sequence[:60], qual((20,)) * 60)])
        runQualityTrim(chas=True)
        self.assertFileLen(lowQualFile, 4, "Chastity on should filter sequences with chastity fail flag")
     
    def test_chastity_N(self):
        writeSeq([('M1:59:TEST:6:1101:2266:2233 1:N:0:GGCGTA', _sequence[:60], qual((20,)) * 60)])
        runQualityTrim(chas=True)
        self.assertFileLen(trimmedFile, 4, "Chastity on should not filter Sequences with chastity pass flag")
         
    def test_nochastity_Y(self):
        writeSeq([('M1:59:TEST:6:1101:2266:2233 1:Y:0:GGCGTA', _sequence[:60], qual((20,)) * 60)])
        runQualityTrim(chas=False)
        self.assertFileLen(trimmedFile, 4, "Chastity off should not filter Sequences with chastity fail flag")
     
    def test_nochastity_N(self):
        writeSeq([('M1:59:TEST:6:1101:2266:2233 1:N:0:GGCGTA', _sequence[:60], qual((20,)) * 60)])
        runQualityTrim(chas=False)
        self.assertFileLen(trimmedFile, 4, "Chastity off should not filter Sequences with chastity pass flag")
 
class TestNBaseCount(TestQualityTrim):
    '''Checks that the N Base counter works'''
     
    def test_n_base3_spread_qual(self):
        seq = list(_sequence[:60])
        qua = qual((21,)) * 60
        seq[5] = 'N'
        seq[22] = 'N'
        seq[48] = 'N'
        writeSeq([('test', "".join(seq), qua)])
        runQualityTrim(nbase=2)
        self.assertFileLen(lowQualFile, 4, "3x N's should fail '-N 2' option")
     
    def test_n_base2_spread_qual(self):
        seq = list(_sequence[:60])
        qua = qual((21,)) * 60
        seq[5] = 'N'
        seq[48] = 'N'
        writeSeq([('test', "".join(seq), qua)])
        runQualityTrim(nbase=2)
        self.assertFileLen(trimmedFile, 4, "2x N's should pass '-N 2' option")
     
    def test_n_base3_spread(self):
        seq = list(_sequence[:60])
        qua = list(qual((21,)) * 60)
        seq[5] = 'N'
        qua[5] = '#'
        seq[22] = 'N'
        qua[22] = '#'
        seq[48] = 'N'
        qua[48] = '#'
        writeSeq([('test', "".join(seq), "".join(qua))])
        runQualityTrim(nbase=2)
        self.assertFileLen(lowQualFile, 4, "3x N's should fail '-N 2' option")
     
    def test_n_base2_spread(self):
        seq = list(_sequence[:60])
        qua = list(qual((21,)) * 60)
        seq[5] = 'N'
        qua[5] = '#'
        seq[48] = 'N'
        qua[48] = '#'
        writeSeq([('test', "".join(seq), "".join(qua))])
        runQualityTrim(nbase=2)
        self.assertFileLen(trimmedFile, 4, "2x N's should pass '-N 2' option")
     
    def test_n_base3_grouped_qual(self):
        seq = list(_sequence[:60])
        qua = qual((21,)) * 60
        seq[21] = 'N'
        seq[22] = 'N'
        seq[23] = 'N'
        writeSeq([('test', "".join(seq), qua)])
        runQualityTrim(nbase=2)
        self.assertFileLen(lowQualFile, 4, "3x N's should fail '-N 2' option")
     
    def test_n_base2_grouped_qual(self):
        seq = list(_sequence[:60])
        qua = qual((21,)) * 60
        seq[22] = 'N'
        seq[23] = 'N'
        writeSeq([('test', "".join(seq), qua)])
        runQualityTrim(nbase=2)
        self.assertFileLen(trimmedFile, 4, "2x N's should pass '-N 2' option")
     
    def test_n_base3_grouped(self):
        seq = list(_sequence[:60])
        qua = list(qual((21,)) * 60)
        seq[21] = 'N'
        qua[21] = '#'
        seq[22] = 'N'
        qua[22] = '#'
        seq[23] = 'N'
        qua[23] = '#'
        writeSeq([('test', "".join(seq), "".join(qua))])
        runQualityTrim(nbase=2)
        self.assertFileLen(lowQualFile, 4, "3x N's should fail '-N 2' option")
     
    def test_n_base2_grouped(self):
        seq = list(_sequence[:60])
        qua = list(qual((21,)) * 60)
        seq[21] = 'N'
        qua[21] = '#'
        seq[22] = 'N'
        qua[22] = '#'
        writeSeq([('test', "".join(seq), "".join(qua))])
        runQualityTrim(nbase=2)
        self.assertFileLen(trimmedFile, 4, "2x N's should pass '-N 2' option")
 
class TestBothEndTrimming(TestQualityTrim):
    '''Tests various scenarios with both end trimming'''
     
    def test_noncont_both(self):
        seq = _sequence[:70]
        qua = list(badbase*5 + goodbase*60 + badbase*5)
        qua[34] = badbase
        qua[35] = badbase
        writeSeq([('test', seq, "".join(qua))])
        runQualityTrim(start=True)
        self.assertFileLen(trimmedFile, 4, "2 bad bases and both bad ends should be accepted")
        self.assertSeqEquals(trimmedFile, _sequence[5:65], "Trimming performed incorrectly")
     
    def test_2segs_short_long(self):
        seq = _sequence[:90]
        qua = badbase*5 + goodbase*20 + badbase*5 + goodbase*55 + badbase*5
        writeSeq([('test', seq, qua)])
        runQualityTrim(start=True)
        self.assertFileLen(trimmedFile, 4, "2 segments should be accepted")
        self.assertSeqEquals(trimmedFile, _sequence[30:85], "Trimming performed incorrectly.  Only long segment should be kept")
     
    def test_2segs_long_short(self):
        seq = _sequence[:90]
        qua = badbase*5 + goodbase*55 + badbase*5 + goodbase*20 + badbase*5
        writeSeq([('test', seq, qua)])
        runQualityTrim(start=True)
        self.assertFileLen(trimmedFile, 4, "2 segments should be accepted")
        self.assertSeqEquals(trimmedFile, _sequence[5:60], "Trimming performed incorrectly.  Only long segment should be kept")
     
    def test_2segs_long_long(self):
        seq = _sequence[:125]
        qua = badbase*5 + goodbase*55 + badbase*5 + goodbase*55 + badbase*5
        writeSeq([('test', seq, qua)])
        runQualityTrim(start=True)
        self.assertFileLen(trimmedFile, 4, "2 segments should be accepted")
        self.assertSeqEquals(trimmedFile, _sequence[5:60], "Trimming performed incorrectly.  First long segment should be kept")
     
    def test_2segs_long_longer(self):
        seq = _sequence[:130]
        qua = badbase*5 + goodbase*55 + badbase*5 + goodbase*60 + badbase*5
        writeSeq([('test', seq, qua)])
        runQualityTrim(start=True)
        self.assertFileLen(trimmedFile, 4, "2 segments should be accepted")
        self.assertSeqEquals(trimmedFile, _sequence[65:125], "Trimming performed incorrectly.  Only longer segment should be kept")
     
    def test_2segs_longer_long(self):
        seq = _sequence[:130]
        qua = badbase*5 + goodbase*60 + badbase*5 + goodbase*55 + badbase*5
        writeSeq([('test', seq, qua)])
        runQualityTrim(start=True)
        self.assertFileLen(trimmedFile, 4, "2 segments should be accepted")
        self.assertSeqEquals(trimmedFile, _sequence[5:65], "Trimming performed incorrectly.  Only longer segment should be kept")
     
    def test_2segs_short_short(self):
        seq = _sequence[:130]
        qua = badbase*5 + goodbase*20 + badbase*5 + goodbase*25 + badbase*5
        writeSeq([('test', seq, qua)])
        runQualityTrim(start=True)
        self.assertFileLen(lowQualFile, 4, "2 segments should be rejected")
     
    def test_start_trim(self):
        seq = _sequence[:70]
        qua = qual(range(10,20)) + greatqual*60
        writeSeq([('test', seq, qua)])
        runQualityTrim(start=True)
        self.assertSeqEquals(trimmedFile, _sequence[5:70], "Start trim should be at base 5 (%s...)" % (_sequence[5:10],))
     
    def test_both_trim(self):
        seq = _sequence[:80]
        qua = qual(range(10,20)) + greatqual*60 + qual(range(20,10,-1))
        writeSeq([('test', seq, qua)])
        runQualityTrim(start=True)
        self.assertSeqEquals(trimmedFile, _sequence[5:76], "Both trim should be at base 5 to 76 (%s..%s)" % (_sequence[5:10],_sequence[71:76]))
 
    def test_low_avg_quality14_with_start(self):
        writeSeq([('test', _sequence[:60], qual((14,)) * 60)])
        runQualityTrim(start=True)
        self.assertFileLen(lowQualFile, 4, "Avg Quality of 14 should fail")
 
    def test_low_avg_quality19p99_with_start(self):
        writeSeq([('test', _sequence[:100], (qual((20,)) * 99) + qual((19,)))])
        runQualityTrim(start=True)
        self.assertFileLen(lowQualFile, 4, "Avg Quality of 19.99 should fail")
     
    def test_low_avg_quality20_with_start(self):
        writeSeq([('test', _sequence[:60], qual((20,)) * 60)])
        runQualityTrim(start=True)
        self.assertFileLen(trimmedFile, 4, "Avg Quality of 20 should pass")
 
class TestMultiFastq(TestQualityTrim):
    '''Test with multiple sequences in fastq file'''
 
    def test_multi_fastq(self):
        seqs = []
         
        seq = list(_sequence[:60])
        qua = list(goodbase * 60)
        seq[5] = 'N'
        qua[5] = '#'
        seq[48] = 'N'
        qua[48] = '#'
        seqs.append(('test1', "".join(seq), "".join(qua)))
         
        seq = list(_sequence[:60])
        qua = list(goodbase * 60)
        seq[5] = 'N'
        qua[5] = '#'
        seq[22] = 'N'
        qua[22] = '#'
        seq[48] = 'N'
        qua[48] = '#'
        seqs.append(('test2', "".join(seq), "".join(qua)))
         
        seqs.append(('test3', _sequence[:60], qual((14,)) * 60))
         
        writeSeq(seqs)
        runQualityTrim(start=True)
        self.assertFileLen(trimmedFile, 8, "Two seqs should pass")
        self.assertFileLen(lowQualFile, 4, "One seq should fail")
         
class TestBufferOverflow(TestQualityTrim):
    '''Checks to make sure it can handle a buffer overflow ok'''
     
    def test_name_overflow126(self):
        nam = "test" + "a"*121
        seq = _sequence[:60]
        qua = goodbase*60
        writeSeq([(nam, seq, qua)])
        rc = runQualityTrim()
        self.assertEqual(rc, 0, "Return code should be 0 with a 126 char name")
        self.assertFileLen(trimmedFile, 4, "1 sequence should pass")
     
    def test_name_overflow127(self):
        nam = "test" + "a"*122
        seq = _sequence[:60]
        qua = goodbase*60
        writeSeq([(nam, seq, qua)])
        rc = runQualityTrim(outzip=2)
        self.assertNotEqual(rc, 0, "Return code should be non-0 with a 127 char name")
        #TODO: check for error message on STDERR
         
    def test_seq_overflow510(self):
        nam = "test"
        seq = _sequence[:102]*5
        qua = goodbase*510
        writeSeq([(nam, seq, qua)])
        rc = runQualityTrim()
        self.assertEqual(rc, 0, "Return code should be 0 with a 510 base seq")
        self.assertFileLen(trimmedFile, 4, "1 sequence should pass")
         
    def test_seq_overflow511(self):
        nam = "test"
        seq = _sequence[:102]*5 + "A"
        qua = goodbase*511
        writeSeq([(nam, seq, qua)])
        rc = runQualityTrim()
        self.assertNotEqual(rc, 0, "Return code should be non-0 with a 511 base seq")
        #TODO: check for error message on STDERR
 
 
class TestGZip(TestQualityTrim):
    '''Tests the GZip input and output functions'''
     
    def test_gzip_to_plain(self):
        seq = _sequence[:60]
        qua = goodbase*60
        writeSeq([('test', seq, qua)], gz=True)
        rc = runQualityTrim(outzip=2)
        rcz = runQualityTrim(checkzip=True)
        if rcz == 0:
            self.assertEqual(rc, 0, "Return code should be 0")
            self.assertFileLen(trimmedFile, 4, "1 sequence should pass")
        else:
            self.assertEqual(rc, 5, "Return code should be 5 when zip not supported")
     
    def test_plain_to_gzip(self):
        seq = _sequence[:60]
        qua = goodbase*60
        writeSeq([('test', seq, qua)])
        rc = runQualityTrim(outzip=1)
        rc = runcmd(['gunzip', trimmedFile + ".gz"])
        rcz = runQualityTrim(checkzip=True)
        if rcz == 0:
            self.assertEqual(rc, 0, "Return code should be 0")
            self.assertEqual(rc, 0, "Gunzip failed to extract result file")
            self.assertFileLen(trimmedFile, 4, "1 sequence should pass")
        else:
            self.assertEqual(rc, 1, "Return code should be 1 when zip not supported")
     
    def test_gzip_to_gzip(self):
        seq = _sequence[:60]
        qua = goodbase*60
        writeSeq([('test', seq, qua)])
        rcz = runQualityTrim(checkzip=True)
        rc = runQualityTrim(outzip=1)
        if rcz == 0:
            runcmd(['gunzip', testInFileNameGZ])
            self.assertEqual(rc, 0, "Return code should be 0")
            rc = runcmd(['gunzip', trimmedFile + ".gz"])
            self.assertEqual(rc, 0, "Gunzip failed to extract result file")
            self.assertFileLen(trimmedFile, 4, "1 sequence should pass")
        else:
            self.assertEqual(rc, 6, "Return code should be 6 when zip not supported")
         
 
class TestMisc(TestQualityTrim):
    '''A few misc test cases'''
     
    def test_empty_sequence(self):
        writeSeq([('test', '', '')])
        self.assertEqual(runQualityTrim(start=True), 0, 'None-zero return code for empty sequence')
 
    def test_plus_annotation(self):
        seq = _sequence[:60]
        qua = goodbase*60
        writeSeq([('test', seq, qua, 'test')])
        self.assertEqual(runQualityTrim(start=True), 0, 'None-zero return code for annotation on "+" line')


class TestGTail(TestQualityTrim):
    '''Checks the gtail trimmer functions correctly'''
    
    def test_good_gtail(self):
        writeSeq([('test', _sequence[:60] + "G"*10, qual((20,))*70) ])
        rc = runQualityTrim(gtail=True)
        self.assertEqual(rc, 0, "Return code should be 0 with a good qual gtail")
        self.assertSeqEquals(trimmedFile, _sequence[:60], "Gtail should be trimmed")
    
    def test_poor_gtail(self):
        writeSeq([('test', _sequence[:60] + "G"*10, (qual((20,))*57) + (qual((14,)) * 13)) ])
        rc = runQualityTrim(gtail=True)
        self.assertEqual(rc, 0, "Return code should be 0 with a poor qual gtail")
        self.assertSeqEquals(trimmedFile, _sequence[:57], "poor qual should be trimmed")
    
    def test_partpoor_gtail(self):
        writeSeq([('test', _sequence[:60] + "G"*10, (qual((20,))*65) + (qual((14,)) * 5)) ])
        rc = runQualityTrim(gtail=True)
        self.assertEqual(rc, 0, "Return code should be 0 with a part poor tail")
        self.assertSeqEquals(trimmedFile, _sequence[:60], "Gtail should be trimmed")
    
    def test_good_nogtail(self):
        writeSeq([('test', _sequence[:60], qual((20,))*60) ])
        rc = runQualityTrim(gtail=True)
        self.assertEqual(rc, 0, "Return code should be 0 with no gtail")
        self.assertSeqEquals(trimmedFile, _sequence[:60], "No Gtail should be trimmed")
    
    def test_good_gtail_lowavgqaul(self):
        writeSeq([('test', _sequence[:60] + "G"*10, (qual((19,))*60) + (qual((40,)) * 10)) ])
        rc = runQualityTrim(gtail=True)
        self.assertEqual(rc, 0, "Return code should be 0 with a low avg qual and high gtail")
        self.assertFileLen(lowQualFile, 4, "1 sequence should fail for low avg qual")


## support functions ##
def writeSeq(seqs, gz=False):
    '''Writes sequences to a file'''
    outFile = open(testInFileName, 'w')
    for seq in seqs:
        outFile.write('@')
        outFile.write(seq[0])
        outFile.write('\n')
        outFile.write(seq[1])
        if len(seq) == 4:
            outFile.write('\n+')
            outFile.write(seq[3])
            outFile.write('\n')
        else:
            outFile.write('\n+\n')
        outFile.write(seq[2])
        outFile.write('\n')
    outFile.close()
    if gz:
        runcmd(['gzip', testInFileName])

def qual(quals):
    '''returns a qualitystring of given (tuple/list) phred scores'''
    res = ""
    for sc in quals:
        res += str(unichr(sc+33))
    return res

greatqual = qual((38,))
goodbase = qual((21,))
avgbase = qual((16,))
badbase = qual((14,))

def runQualityTrim(chas=None, nbase=None, start=None, outzip=None, checkzip=None, gtail=False):
    '''Runs the program'''
    if os.path.exists(testInFileNameGZ):
        infile = testInFileNameGZ
    else:
        infile = testInFileName
    
    args = [qualityTrimApp]
    if chas==False:
        args.append('-n')
    if nbase is not None:
        args.append('-N %s'%nbase)
    if start == True:
        args.append('-s')
    if outzip is not None:
        args.append('-z %s' % outzip)
    if checkzip == True:
        args.append('-Z')
    if gtail:
        args.append("-g")
    args.append(infile)
    return runcmd(args)

def runcmd(cmd):
    '''Runs a command on the commandline.  Expects a list containing the cmd and any arguments'''
    fnull = open(os.devnull, 'w')
    if fnull:
        if False:
            sc = subprocess.call(cmd)
        else:
            sc = subprocess.call(cmd, stdout=fnull, stderr=subprocess.STDOUT)
        fnull.close()
        return sc
    else:
        raise Exception("Unable to hide output of run")
    return 222

## test runner ##
if __name__ == '__main__':
    unittest.main()
