#
# Builds quality-trim
#
# Compile:
# - make
#   This will place the executable in the bin/ directory which you can install where you like
#
# Use libargtable2 instead of getopt
# - make ARGTABLE=Y
#
# Add support for Gzip files
# - make GZIP=Y
# - make GZIP=OLD   # (for libz 1.2.5 and below)
#
# Author: Andrew Robinson
#

VERSION=1.6.0

# conditional includes/libs
CLIBS_ARGTABLE_Y=-largtable2
CLIBS_ARGTABLE_=
CLIBS_GZIP_Y=-lz
CLIBS_GZIP_OLD=-lz
CFLAGS_ARGTABLE_Y=-DUSE_ARGTABLE
CFLAGS_GZIP_Y=-DUSE_ZLIB
CFLAGS_GZIP_OLD=-DUSE_ZLIB_OLD

CLIBS=$(CLIBS_ARGTABLE_$(ARGTABLE)) $(CLIBS_GZIP_$(GZIP))
CFLAGS=$(CFLAGS_ARGTABLE_$(ARGTABLE)) $(CFLAGS_GZIP_$(GZIP)) -DVERSION="\"$(VERSION)\""

# master includes/libs/options
INCLUDE=
OPTS=-O3 -Wall

# targets
all: prep quality-trim

test:
	python test_src/TestQualityTrim.py

testad:
	python test_src/TestAdaptorTrim.py

prep:
	mkdir -p build bin

build/QualityTrim.o: src/QualityTrim.c
	gcc $(OPTS) $(CFLAGS) -o build/QualityTrim.o -c src/QualityTrim.c $(INCLUDE)

quality-trim: build/QualityTrim.o
	gcc  -o bin/quality-trim  ./build/QualityTrim.o  $(CLIBS)

clean:
	rm -f build/* bin/*

# EOF #
