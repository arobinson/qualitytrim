/*******************************************************************************
 *   (c) Andrew Robinson (andrew.robinson@latrobe.edu.au) 2012-2013            *
 *       La Trobe University &                                                 *
 *       Life Sciences Computation Centre (LSCC, part of VLSCI)                *
 *                                                                             *
 *  This file is part of QualityTrim.                                          *
 *                                                                             *
 *  QualityTrim is free software: you can redistribute it and/or modify        *
 *  it under the terms of the GNU Lesser General Public License as published   *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  QualityTrim is distributed in the hope that it will be useful,             *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU Lesser General Public License for more details.                        *
 *                                                                             *
 *  You should have received a copy of the GNU Lesser General Public License   *
 *  along with QualityTrim.  If not, see <http://www.gnu.org/licenses/>.       *
 *                                                                             *
 *******************************************************************************/

/**
 * QualityTrim.c
 *
 *  Created on: 14/02/2012
 *      Author: Andrew Robinson
 *      
 *  Algorithm based on that of trimming.py (by Elizabeth Ross).
 *  
 *  End only trimming:
 *  - Checks Chastity (if required)
 *  - Scans sequence for X bad bases and chops here
 *  - Checks average quality passes
 *  - Checks length remaining passes
 *  - If all ok then writes to pass file (otherwise to required fail file)
 *  
 *  Start and end trimming:
 *  - Checks Chastity (if required)
 *  - Scans for first good base
 *  - Scans for X bad bases and stores hit (if passes):
 *    - Average quality
 *    - Min Length
 *  - Keeps best (longest) match or writes to low quality file 
 *  
 *  
 *  Known issues:
 *  - doesn't handle random bad bases spread though the sequence well
 *    e.g. ------++++-++++++++++++++++++-++++++++-++++++++++++++++++++++++++++---
 *    matches    S                              E S                          E 
 *    both would fail cutoff of 32
 *                    S                                                      E (len > cutoff)
 *    yet this piece exists above 32
 *  -  
 */

#include <stdio.h>
#include <string.h>

// testing definitions (TODO: comment these before release)
//#define USE_ARGTABLE
//#define USE_ZLIB
//#define USE_ZLIB_OLD

#ifdef USE_ZLIB
# define USE_ZLIB_ANY
#else
# ifdef USE_ZLIB_OLD
#  define USE_ZLIB_ANY
# endif
#endif

#ifndef VERSION
#define VERSION "dev"
#endif


#ifdef USE_ARGTABLE
#include <argtable2.h>
#else
#include <stdlib.h>
#include <unistd.h>
#endif

#ifdef USE_ZLIB
	#include <zlib.h>

	// type/function wrappers
	#define FILE_PTR gzFile
	#define FILE_PTR_DECL(_name_) FILE_PTR _name_
	#define FILE_PTR_INIT(_arr_, _size_) {int i = 0; for (; i < _size_; i++) {_arr_[i] = NULL;}};
	#define OPENREAD(filehandle, filename, _gz_) filehandle = gzopen(filename, "r");
	#define OPENWRITEGZ(filehandle, filename) filehandle = gzopen(filename, "wb");
	#define OPENWRITE(filehandle, filename) filehandle = gzopen(filename, "wbT");
	#define NOTOPEN(file) file == NULL
	#define ISOPEN(file) file != NULL
	#define EXTENSIONGZ ".fastq.gz"
	#define EXTENSION ".fastq"
	#define GETS(str, len, file) gzgets(file, str, len)
	#define PUTS(str, file) gzputs(file, str)
	#define CLOSE(file) gzclose(file)
#else
#ifdef USE_ZLIB_OLD
	#include <zlib.h>

	/**
	 * A struct to store the file handles for both types of file when libz 
	 * doesn't support write-through (i.e. libz-1.2.5 and below)
	 */
	struct FileHandleGroup {
		short gz;
		gzFile gzfile;
		FILE *plainfile;
	};

	// type/function wrappers
	#define FILE_PTR struct FileHandleGroup
	#define FILE_PTR_DECL(_name_) FILE_PTR (_name_)
	#define FILE_PTR_INIT(_arr_, _size_) {int i = 0; for (; i < _size_; i++) {_arr_[i].gzfile = NULL;_arr_[i].plainfile = NULL;}};
	#define OPENREAD(filehandle, filename, _gz_) filehandle.gz = _gz_;\
		if(filehandle.gz==1)filehandle.gzfile = _OPENREADGZ(filename) else filehandle.plainfile = _OPENREADPL(filename);
	#define _OPENREADGZ(filename) gzopen(filename, "r");
	#define _OPENREADPL(filename) fopen(filename, "r");
	#define OPENWRITE(filehandle, filename) filehandle.gz = 0; filehandle.plainfile = _OPENWRITEPL(filename);
	#define OPENWRITEGZ(filehandle, filename) filehandle.gz = 1; filehandle.gzfile = _OPENWRITEGZ(filename);
	#define _OPENWRITEGZ(filename) gzopen(filename, "wb");
	#define _OPENWRITEPL(filename) fopen(filename, "wb");
	#define NOTOPEN(file) (file.gz==1?file.gzfile==NULL:file.plainfile== NULL)
	#define ISOPEN(file) (file.gz==1?file.gzfile!=NULL:file.plainfile!= NULL)
	#define EXTENSIONGZ ".fastq.gz"
	#define EXTENSION ".fastq"
	#define GETS(str, len, file) (file.gz==1?_GETSGZ(str, len, file.gzfile):_GETSPL(str, len, file.plainfile))
	#define _GETSGZ(str, len, file) gzgets(file, str, len)
	#define _GETSPL(str, len, file) fgets(str, len, file)
	#define PUTS(str, file) (file.gz==1?_PUTSGZ(str, file.gzfile):_PUTSPL(str, file.plainfile))
	#define _PUTSGZ(str, file) gzputs(file, str)
	#define _PUTSPL(str, file) fputs(str, file)
	#define CLOSE(file) (file.gz==1?_CLOSEGZ(file.gzfile):_CLOSEPL(file.plainfile))
	#define _CLOSEGZ(file) gzclose(file)
	#define _CLOSEPL(file) fclose(file)

#else
	// type/function wrappers
	#define FILE_PTR FILE *
	#define FILE_PTR_DECL(_name_) FILE_PTR _name_
	#define FILE_PTR_INIT(_arr_, _size_) {int i = 0; for (; i < _size_; i++) {_arr_[i] = NULL;}};
	#define OPENREAD(filehandle, filename, _gz_) if (_gz_==0)filehandle = fopen(filename, "r");else filehandle=NULL;
	#define OPENWRITEGZ(filehandle, filename) filehandle=NULL;
	#define OPENWRITE(filehandle, filename) filehandle = fopen(filename, "w");
	#define NOTOPEN(file) file == NULL
	#define ISOPEN(file) file != NULL
	#define EXTENSIONGZ ".fastq"
	#define EXTENSION ".fastq"
	#define GETS(str, len, file) fgets(str, len, file)
	#define PUTS(str, file) fputs(str, file)
	#define CLOSE(file) fclose(file)
#endif
#endif

	

// lengths of buffers.  Actual accepted chars is 2 less(for \n\0).
#define NAME_BUFFER_SIZE 	128
#define SEQ_BUFFER_SIZE 	512

//NOTE: these are ordered (if a sequence has multiple hits then the higher numbered trim status 
//      will overrule even if its shorter otherwise it goes on length).
#define TRIM_UNTRIM	0
#define TRIM_CHAST	1	// trim status only
#define TRIM_LEN	2
#define TRIM_AVGQU	3
#define TRIM_N		4
#define TRIM_SING	5
#define TRIM_LOWQUAL 6	// File Only
#define TRIM_OK		7
#define TRIM_MAX	TRIM_OK

#define FILE_PAIR1	0
#define FILE_OK		FILE_PAIR1
#define FILE_PAIR2	1
#define FILE_SING	2
#define FILE_LOWQUAL	3
#define FILE_MAX	FILE_LOWQUAL

// the string to append to description for each TRIM_* (i.e. TRIM_MAX must be a valid index of this array)
char* statusStr[] = {""," [Chastity]\n"," [Length]\n", " [Average Quality]\n", " [N Bases]\n","","",""};



struct Sequence {
	
	char name[NAME_BUFFER_SIZE+20]; // allow room for longest status string (18 + 2)
	char seq[SEQ_BUFFER_SIZE+2]; 	// allow for \n and \0
	char plus[NAME_BUFFER_SIZE+2];
	char qual[SEQ_BUFFER_SIZE+2];
	unsigned short start;
	unsigned short end;
	unsigned short nBases;
	
	unsigned short trimstatus;
};

struct Options {
	
	unsigned short minQuality;
	unsigned short minAvgQuality;
	unsigned short minLength;
	unsigned short maxPoorBases;
	short maxNBases;
	short tailGCheck;
	int noChastity;
	unsigned short gz;
	unsigned short qualOffset;
	unsigned short trimStart;
};

///**
// * Cleans all data out of the given sequence
// */
//int cleanSequence(struct Sequence * seq)
//{
//	seq->name[0] = 0;
//	seq->seq[0] = 0;
//	seq->plus[0] = 0;
//	seq->qual[0] = 0;
//	seq->start = 0;
//	seq->end = 0;
//	seq->nBases = 0;
//	seq->trimstatus = TRIM_UNTRIM;
//	
//	return 0;
//}

#define CLEAN_SEQ(seqx) seqx.name[0] = 0;\
seqx.seq[0] = 0;\
seqx.plus[0] = 0;\
seqx.qual[0] = 0;\
seqx.start = 0;\
seqx.end = 0;\
seqx.nBases = 0;\
seqx.trimstatus = TRIM_UNTRIM;

/**
 * Prints the given sequence
 */
int printSequence(struct Sequence * seq)
{
	printf("Name: %s\n", seq->name);
	printf("Seq:  %s\n", seq->seq);
	printf("Qual: %s\n", seq->qual);
	printf("\n");
	
	return 0;
}

/**
 * Reads a FastQ sequence from the given file and stores result in seq.
 */
int readSequence(FILE_PTR pSeqFile, struct Sequence * seq)
{
	if (GETS(seq->name, NAME_BUFFER_SIZE, pSeqFile) != NULL
			&& GETS(seq->seq, SEQ_BUFFER_SIZE, pSeqFile) != NULL
			&& GETS(seq->plus, NAME_BUFFER_SIZE, pSeqFile) != NULL
			&& GETS(seq->qual, SEQ_BUFFER_SIZE, pSeqFile) != NULL)
	{
		if (seq->name[0] != '@' || seq->plus[0] != '+') //  || seq->plus[1] != '\n' #AR: removed check to allow annotation on '+' line
			return -4; // out of sync
		return 0; // success
	}
	if (seq->name[0] == '\n')
		return -2; // extra eol
	return -1; // eof
}

/**
 * Checks the the quality of a section of sequence matches criteria.  If its a 
 * 2nd+ positive hit then stores it only if *better* than last.
 */
int checkHit(struct Sequence * seq, struct Options * opt, unsigned start, unsigned end, unsigned short qualitySum, unsigned short nBases, unsigned lastNonG)
{
	unsigned short quality = 0;
	unsigned i = end, gi = end;
	unsigned short len = 0;
	unsigned short base = 0;
	unsigned short qualOffset = opt->qualOffset;
	unsigned short minQuality = opt->minQuality + qualOffset;
	unsigned short trimstatus = TRIM_OK;
	
	/// Calculate trim status ///
	if (i < opt->minLength) // length (before back tracking)
		trimstatus = TRIM_LEN;
	else {

		// find last non-G base (if needed)
		if (opt->tailGCheck) {
			while (seq->qual[gi] >= qualOffset) {
				if (seq->seq[gi] != 'G')
					lastNonG = gi;
				gi++;
			}

			// Back track until last good base or last non-G
			--i;
			quality = seq->qual[i];
			while ((quality < minQuality && i > start) || i > lastNonG)
			{
				qualitySum -= quality;

				--i;
				quality = seq->qual[i];

				if (opt->maxNBases >= 0) {
					base = seq->seq[i];
					if (base == 'N' || base == 'n')
						nBases--;
				}
			}
		}
		else
		{
			// Back track until last good base
			--i;
			quality = seq->qual[i];
			while (quality < minQuality && i > start)
			{
				qualitySum -= quality;

				--i;
				quality = seq->qual[i];

				if (opt->maxNBases >= 0) {
					base = seq->seq[i];
					if (base == 'N' || base == 'n')
						nBases--;
				}
			}
		}

		// do rest of checks
		len = (i - start + 1);
		if (len < opt->minLength) // length (after back tracking)
			trimstatus = TRIM_LEN;
		else if (opt->maxNBases >= 0 && nBases > opt->maxNBases) // max N bases included
			trimstatus = TRIM_N;
		else {

			// check for low average quality
			double avgQual = (qualitySum / (double) len) - qualOffset;
			if (avgQual < opt->minAvgQuality)
				trimstatus = TRIM_AVGQU;
		}	
	}
	
	/// Check if this is a better match and record trim ///
	if (trimstatus > seq->trimstatus  // better type of match (@see note with TRIM_* definitions at top)
			|| (trimstatus == seq->trimstatus && (seq->end - seq->start + 1) < len)) // or same and longer
	{
		seq->start = start;
		seq->end = i;
		seq->trimstatus = trimstatus;
	}
	
	return 0;
}


/**
 * Checks (and trims if needed) a Sequence
 */
int qualityCheckSequence(struct Sequence * seq, struct Options * opt)
{
	unsigned qualitySum = 0;
	unsigned short poorBases = 0;
	unsigned short qualOffset = opt->qualOffset;
	unsigned short minQuality = opt->minQuality + qualOffset; 
	unsigned i = 0;
	unsigned short base = 0;
	unsigned short quality = 0;
	char chastitySearchChars[] = {' ',':'};
	unsigned short chastityLen = 2;
	unsigned short chastityPos = 0;
	unsigned short namePos = 0;
	char chastityFailChar = 'Y';
	unsigned short searchStatus = 0; // 0 = searching for start, 1 = searching for end
	unsigned startBase = 0;
	unsigned short nBases = 0;
	unsigned lastNonG = 0;
	unsigned short baseChecks = opt->maxNBases >= 0 || opt->tailGCheck > 0;
	
	
	// check Ilumina Chastity
	if (opt->noChastity == 0)
	{
		while (chastityPos < chastityLen && seq->name[namePos] != '\0')
		{
			if (seq->name[namePos] == chastitySearchChars[chastityPos])
				chastityPos++;
			namePos++;
		}
	}
	if (opt->noChastity == 0 && seq->name[namePos] == chastityFailChar)
		seq->trimstatus = TRIM_CHAST;
	else
	{
		if (opt->trimStart)
		{
			quality = seq->qual[i]; // note: quality is the ascii value (i.e. quality 33 = phred 0)
			while (quality >= qualOffset) // note: quality == 0 at end of line
			{
				if (searchStatus == 0) { // finding start
					if (quality >= minQuality) {
						startBase = i;
						searchStatus = 1;
						qualitySum = quality;
					}
				}
				else // finding end
				{
					qualitySum += quality;
					
					// check for N's (if required)
					if (baseChecks) {
						base = seq->seq[i];
						if (base == 'N' || base == 'n')
							nBases++;
						else if (base != 'G')
							lastNonG = i;
					}
					
					if (quality < minQuality)
					{
						poorBases++;
						
						if (poorBases > opt->maxPoorBases)
						{
							checkHit(seq, opt, startBase, i, qualitySum, nBases, lastNonG);
							
							// try again
							searchStatus = 0;
							poorBases = 0;
							qualitySum = 0;
							nBases = 0;
						}
					}
				}
				
				// get next quality
				i++;
				quality = seq->qual[i];
			}
			// check the last hit (that ended rather than entered bad section)
			if (searchStatus == 1)
				checkHit(seq, opt, startBase, i, qualitySum, nBases, lastNonG);
			
			// if its still un-trimmed then it never got a good quality base
			if (seq->trimstatus == TRIM_UNTRIM)
				seq->trimstatus = TRIM_LEN;
		}
		else
		{
			// check for m'th poor base (or end)
			quality = seq->qual[i]; // note: quality is the ascii value (i.e. quality 33 = phred 0)
			while (quality >= qualOffset && poorBases <= opt->maxPoorBases) // note: quality == 0 at end of line
			{
				if (quality < minQuality)
				{
					poorBases++;
				}
				
				qualitySum += quality;
				
				// check for N's (if required)
				if (baseChecks) {
					base = seq->seq[i];
					if (base == 'N' || base == 'n')
						nBases++;
					else if (base != 'G')
						lastNonG = i;
				}
				
				// get next quality
				i++;
				quality = seq->qual[i];
			}
			
			checkHit(seq, opt, 0, i, qualitySum, nBases, lastNonG);
		}
		
		// trim the sequence (i.e. put end of string character after last good base)
		if (seq->trimstatus == TRIM_OK) {
			seq->qual[seq->end + 1] = '\n';
			seq->seq[seq->end + 1]  = '\n';
			seq->qual[seq->end + 2] = 0;
			seq->seq[seq->end + 2]  = 0;
		}
	}
	return 0;
}

/**
 * writes a sequence to the required file
 */
int inline writeSequence(struct Sequence * seq, FILE_PTR file) //, short status)
{
	if (ISOPEN(file))
	{
		if (statusStr[seq->trimstatus][0] != 0) {
			size_t namelen = strlen(seq->name); // trim EOL
			if (namelen > 1)
				seq->name[namelen - 1] = 0;
			PUTS(seq->name, file);
			PUTS(statusStr[seq->trimstatus],file);
		}
		else
			PUTS(seq->name, file);
		PUTS(&(seq->seq[seq->start]), file);
		PUTS(seq->plus, file);
		PUTS(&(seq->qual[seq->start]), file);
	}
	return 0;
}

/**
 * A macro version of writeSequence to improve performance
 */
#define WRITE_SEQ(seqx, file) if (ISOPEN(file))\
{\
	if (statusStr[seqx.trimstatus][0] != 0) {\
		size_t namelen = strlen(seqx.name);\
		if (namelen > 1)\
			seqx.name[namelen - 1] = 0;\
		PUTS(seqx.name, file);\
		PUTS(statusStr[seqx.trimstatus],file);\
	}\
	else\
		PUTS(seqx.name, file);\
	PUTS(&(seqx.seq[seqx.start]), file);\
	PUTS(seqx.plus, file);\
	PUTS(&(seqx.qual[seqx.start]), file);\
}

/**
 * Initialises array of file pointers
 */
void initFiles(FILE * files[], short count)
{
	memset(files, 0, sizeof(FILE*) * count);
}

/**
 * Closes the files provided
 */
void closeFiles1(short count, FILE_PTR src1, FILE_PTR files1[])
{
	int i;
	
	if (ISOPEN(src1))
		CLOSE(src1);
	
	if (files1 != NULL) {
		for (i = 0; i <= count; i++)
		{
			if (ISOPEN(files1[i]))
				CLOSE(files1[i]);
		}
	}
}

/**
 * Closes the files provided
 */
void closeFiles(short count, FILE_PTR src1, FILE_PTR files1[], FILE_PTR src2)
{
	int i;
	
	if (ISOPEN(src1))
		CLOSE(src1);
	
	if (ISOPEN(src2))
		CLOSE(src2);
	
	if (files1 != NULL) {
		for (i = 0; i <= count; i++)
		{
			if (ISOPEN(files1[i]))
				CLOSE(files1[i]);
		}
	}
}

/**
 * Extracts the length of the extension (if none found 0).
 */
int extLen(const char *filename, size_t length)
{
	//int length=strlen(filename);
	int i = length - 1;
	int c = 0;
	for (; i >= 0; i--)
	{
		c++;
		if (filename[i] == '.')
		{
			if (!(i == length - 3 && filename[i+1] == 'g' && filename[i+2] == 'z'))
				return c;
		}
	}
	
	return 0;
}

/**
 * Performs Quality Trimming on an optional paired reads.
 */
int qualityTrim(char ** files, 
		short filecount,
		unsigned short minQuality, 
		unsigned short minAvgQuality,
		unsigned short minLength,
		unsigned short maxPoorBases, 
		short tailGCheck,
		int noChastity,
		short overridegz,
		short taboutput,
		unsigned short qualOffset,
		unsigned short trimStart,
		short maxNBases)
{
	
	FILE_PTR_DECL(pSeqFile1);
	FILE_PTR_DECL(pSeqFile2);
	FILE_PTR_DECL(pOutFiles)[FILE_MAX+1];
	FILE_PTR_INIT(pOutFiles, FILE_MAX+1)

	FILE_PTR_DECL(pOutFileMap1)[TRIM_MAX+1];
	FILE_PTR_INIT(pOutFileMap1, FILE_MAX+1)
	FILE_PTR_DECL(pOutFileMap2)[TRIM_MAX+1];
	FILE_PTR_INIT(pOutFileMap2, FILE_MAX+1)
	
	struct Sequence seq1;
	struct Sequence seq2;
	struct Options opts;
	unsigned counters[TRIM_MAX+1];
	short paired = 0;
	short i;
	short returncode = 0;
	const char* file1;
	const char* file2;

	// initialise vars
	
	// check if we are doing paired reads or not
	if (filecount > 1)
		paired = 1;
	else if (filecount > 0)
		paired = 0;
	else
		return 4;
	
	file1 = files[0];
	file2 = files[1];
	
	// print summary of options
	if (taboutput == 0)
	{
		printf("Quality trimming:\n");
		printf("Mode:                     %s\n", (paired == 1?"paired":"single"));
	#ifdef USE_ZLIB
		printf("Compression:              ZLIB\n");
	#else
		printf("Compression:              None\n");
	#endif
		printf("Min Quality:              %i\n", minQuality);
		printf("Min Avg Quality:          %i\n", minAvgQuality);
		printf("Min Length:               %i\n", minLength);
		printf("Max Poor Bases:           %i\n", maxPoorBases);
		printf("Max N Bases:              %i\n", maxNBases);
		printf("Chastity filter:          %s\n", (noChastity == 1?"No":"Yes"));
		if (trimStart == 1)
			printf("Trim start:               Yes\n");
		else
			printf("Trim start:               No\n");
		if (tailGCheck == 1)
			printf("Trim tail Gs:             Yes\n");
		else
			printf("Trim tail Gs:             No\n");
		printf("\nFile(s):\n");
		printf("- %s\n", file1);
		if (paired==1)
			printf("- %s\n", file2);
	}
	
	//return 0;
	opts.minQuality = minQuality;
	opts.minAvgQuality = minAvgQuality;
	opts.minLength = minLength;
	opts.maxPoorBases = maxPoorBases;
	opts.maxNBases = maxNBases;
	opts.tailGCheck = tailGCheck;
	opts.noChastity = noChastity;
	opts.gz = 0;
	opts.qualOffset = qualOffset;
	opts.trimStart = (trimStart > 0? 1: 0);

	// clear counters
	memset(counters,(unsigned)0, sizeof(counters));

	if (paired==1) // paired
	{
		int filelen = strlen(file1);
		int extlen = extLen(file1, filelen);
		int offset = filelen - extlen;
		int filelen2 = strlen(file2);
		int extlen2 = extLen(file2, filelen2);
		int offset2 = filelen2 - extlen2;
		char filename[filelen+19];
		char filename2[filelen2+19];
		int readstatus1 = 0;
		int readstatus2 = 0;
		int seqcount = 0;
		
		// check for zipped input files
		if (filelen > 2 && file1[filelen-3] == '.' && file1[filelen-2] == 'g' && file1[filelen-1] == 'z')
			opts.gz = 1;
		
		// open file
		OPENREAD(pSeqFile1, file1, opts.gz);
		OPENREAD(pSeqFile2, file2, opts.gz);
		if (NOTOPEN(pSeqFile1) || NOTOPEN(pSeqFile2)) {
			if (NOTOPEN(pSeqFile1))
				printf("Unable to open: %s\n", file1);
			if (NOTOPEN(pSeqFile2))
				printf("Unable to open: %s\n", file2);
			closeFiles(0, pSeqFile1, NULL, pSeqFile2);
			return 5;
		} else {

			if (overridegz != 0)
				opts.gz = (overridegz == 1?1:0);
			
			// initialise
			CLEAN_SEQ(seq1);
			CLEAN_SEQ(seq2);
			
			// open output files
			strcpy(filename, file1);
			strcpy(filename2, file2);

			if (taboutput == 0)
				printf("\nOutfiles:\n");
			
			// open failed files
			if (opts.gz == 1)
			{
				if (taboutput == 0)
					printf("+ Write mode:             gzip\n");
				
				strcpy(&(filename[offset]), "-trimmed" EXTENSIONGZ);
				if (taboutput == 0)
					printf("- Trimmed 1:              %s\n", filename);
				OPENWRITEGZ(pOutFiles[FILE_PAIR1], filename);
				strcpy(&(filename2[offset2]), "-trimmed" EXTENSIONGZ);
				if (taboutput == 0)
					printf("- Trimmed 2:              %s\n", filename2);
				OPENWRITEGZ(pOutFiles[FILE_PAIR2], filename2);
				
				// construct filename for paired
				int i = 0;
				for(i = 0; i < offset; i++)
					if (filename[i] != filename2[i])
						filename[i] = 'X';
				
				strcpy(&(filename[offset]), "-singleton" EXTENSIONGZ);
				if (taboutput == 0)
					printf("- Singleton:              %s\n", filename);
				OPENWRITEGZ(pOutFiles[FILE_SING], filename);
				strcpy(&(filename[offset]), "-lowquality" EXTENSIONGZ);
				if (taboutput == 0)
					printf("- Low Quality:            %s\n", filename);
				OPENWRITEGZ(pOutFiles[FILE_LOWQUAL], filename);
			}
			else
			{
				if (taboutput == 0)
					printf("+ Write mode:             plain\n");
				
				strcpy(&(filename[offset]), "-trimmed" EXTENSION);
				if (taboutput == 0)
					printf("- Trimmed 1:              %s\n", filename);
				OPENWRITE(pOutFiles[FILE_PAIR1], filename);
				strcpy(&(filename2[offset2]), "-trimmed" EXTENSION);
				if (taboutput == 0)
					printf("- Trimmed 2:              %s\n", filename2);
				OPENWRITE(pOutFiles[FILE_PAIR2], filename2);
				
				// construct filename for paired
				int i = 0;
				for(i = 0; i < offset; i++)
					if (filename[i] != filename2[i])
						filename[i] = 'X';
				
				strcpy(&(filename[offset]), "-singleton" EXTENSION);
				if (taboutput == 0)
					printf("- Singleton:              %s\n", filename);
				OPENWRITE(pOutFiles[FILE_SING], filename);
				strcpy(&(filename[offset]), "-lowquality" EXTENSION);
				if (taboutput == 0)
					printf("- Low Quality:            %s\n", filename);
				OPENWRITE(pOutFiles[FILE_LOWQUAL], filename);
			}

			if (NOTOPEN(pOutFiles[FILE_PAIR1])
					|| NOTOPEN(pOutFiles[FILE_PAIR2])
					|| NOTOPEN(pOutFiles[FILE_SING])
					|| NOTOPEN(pOutFiles[FILE_LOWQUAL]))
			{
				printf("Failed to open one or more output files for writing!\n");
				closeFiles(FILE_MAX, pSeqFile1, pOutFiles, pSeqFile2);
				return 6;
			}
			
			
			// map trim status to files
			pOutFileMap1[TRIM_OK] = pOutFiles[FILE_PAIR1];
			pOutFileMap2[TRIM_OK] = pOutFiles[FILE_PAIR2];
			pOutFileMap1[TRIM_SING] = pOutFiles[FILE_SING];
			pOutFileMap2[TRIM_SING] = pOutFiles[FILE_SING];
			
			pOutFileMap1[TRIM_CHAST] = pOutFiles[FILE_LOWQUAL];
			pOutFileMap2[TRIM_CHAST] = pOutFiles[FILE_LOWQUAL];
			pOutFileMap1[TRIM_LEN] = pOutFiles[FILE_LOWQUAL];
			pOutFileMap2[TRIM_LEN] = pOutFiles[FILE_LOWQUAL];
			pOutFileMap1[TRIM_AVGQU] = pOutFiles[FILE_LOWQUAL];
			pOutFileMap2[TRIM_AVGQU] = pOutFiles[FILE_LOWQUAL];
			pOutFileMap1[TRIM_N] = pOutFiles[FILE_LOWQUAL];
			pOutFileMap2[TRIM_N] = pOutFiles[FILE_LOWQUAL];

			if (taboutput == 0)
				printf("\nRunning ...");
			fflush(stdout);
			
			// read each sequence (pair)
			readstatus1 = readSequence(pSeqFile1, &seq1);
			readstatus2 = readSequence(pSeqFile2, &seq2);
			while (readstatus1 == 0 && readstatus2 == 0)
			{
				// quality check 
				qualityCheckSequence(&seq1, &opts);
				qualityCheckSequence(&seq2, &opts);
				
				// alter status of single reads
				if (seq1.trimstatus != seq2.trimstatus)
				{
					if (seq1.trimstatus == TRIM_OK)
						seq1.trimstatus = TRIM_SING;
					else if (seq2.trimstatus == TRIM_OK)
						seq2.trimstatus = TRIM_SING;
				}
				
				// write sequences
				WRITE_SEQ(seq1, pOutFileMap1[seq1.trimstatus]);
				WRITE_SEQ(seq2, pOutFileMap2[seq2.trimstatus]);
				counters[seq1.trimstatus]++;
				counters[seq2.trimstatus]++;
				seqcount++;
				
				// cleanup
				CLEAN_SEQ(seq1);
				CLEAN_SEQ(seq2);
				
				// read next sequence
				readstatus1 = readSequence(pSeqFile1, &seq1);
				readstatus2 = readSequence(pSeqFile2, &seq2);
			}
			
			switch (readstatus1) {
			case -2:
				printf("\nWARNING: extra newline at end of file 1\n");
				returncode = 7;
				break;
			case -4:
				printf("\nERROR: out of sync near line %i file 1\n", seqcount * 4 +1);
				printf("INFO:  could be caused by too long seq(>%i) or seqname(>%i)\n", SEQ_BUFFER_SIZE-2, NAME_BUFFER_SIZE-2);
				returncode = 8;
				break;
			}
			switch (readstatus2) {
			case -2:
				printf("\nWARNING: extra newline at end of file 2\n");
				returncode = 7;
				break;
			case -4:
				printf("\nERROR: out of sync near line %i file 2\n", seqcount * 4 +1);
				printf("INFO:  could be caused by too long seq(>%i) or seqname(>%i)\n", SEQ_BUFFER_SIZE-2, NAME_BUFFER_SIZE-2);
				returncode = 8;
				break;
			}
			
			// close files
			closeFiles(FILE_MAX, pSeqFile1, pOutFiles, pSeqFile2);
		}
	}
	else		// singleton
	{
		int filelen = strlen(file1);
		int extlen = extLen(file1, filelen);
		int offset = filelen - extlen;
		char filename[filelen+19];
		int readstatus1 = 0;
		int seqcount = 0;

		// check for zipped input files
		if (filelen > 2 && file1[filelen-3] == '.' && file1[filelen-2] == 'g' && file1[filelen-1] == 'z')
			opts.gz = 1;
		
		// open file
		OPENREAD(pSeqFile1, file1, opts.gz);
		if (NOTOPEN(pSeqFile1)) {
			printf("Unable to open: %s\n", file1);
			return 5;
		} else {
			
			if (overridegz != 0)
				opts.gz = (overridegz == 1?1:0);

			// initialise
			CLEAN_SEQ(seq1);
			
			// open output files
			strcpy(filename, file1);

			if (taboutput == 0)
				printf("\nOutfiles:\n");
			if (opts.gz == 1)
			{
				strcpy(&(filename[offset]), "-trimmed" EXTENSIONGZ);
				if (taboutput == 0)
					printf("- Trimmed:                %s\n", filename);
				OPENWRITEGZ(pOutFiles[FILE_OK], filename);
				strcpy(&(filename[offset]), "-lowquality" EXTENSIONGZ);
				if (taboutput == 0)
					printf("- Low Quality:            %s\n", filename);
				OPENWRITEGZ(pOutFiles[FILE_LOWQUAL], filename);
			}
			else
			{
				strcpy(&(filename[offset]), "-trimmed" EXTENSION);
				if (taboutput == 0)
					printf("- Trimmed:                %s\n", filename);
				OPENWRITE(pOutFiles[FILE_OK], filename);
				strcpy(&(filename[offset]), "-lowquality" EXTENSION);
				if (taboutput == 0)
					printf("- Low Quality:            %s\n", filename);
				OPENWRITE(pOutFiles[FILE_LOWQUAL], filename);
			}
			
			if (NOTOPEN(pOutFiles[FILE_OK])
					|| NOTOPEN(pOutFiles[FILE_LOWQUAL])
					)
			{
				printf("Failed to open one or more output files for writing!\n");
				closeFiles1(FILE_MAX, pSeqFile1, pOutFiles);
				return 6;
			}
			
			// map trim status to files
			pOutFileMap1[TRIM_OK] = pOutFiles[FILE_OK];
			
			pOutFileMap1[TRIM_CHAST] = pOutFiles[FILE_LOWQUAL];
			pOutFileMap1[TRIM_LEN] = pOutFiles[FILE_LOWQUAL];
			pOutFileMap1[TRIM_AVGQU] = pOutFiles[FILE_LOWQUAL];
			pOutFileMap1[TRIM_N] = pOutFiles[FILE_LOWQUAL];

			if (taboutput == 0)
				printf("\nRunning ...");
			fflush(stdout);
			
			// read each sequence
			while ((readstatus1 = readSequence(pSeqFile1, &seq1)) == 0)
			{
				qualityCheckSequence(&seq1, &opts);
				
				// testing
				//printSequence(&seq1);
				
				// write to file
				WRITE_SEQ(seq1, pOutFileMap1[seq1.trimstatus]);
				counters[seq1.trimstatus]++;
				seqcount++;
				
				// cleanup
				CLEAN_SEQ(seq1);
			}
			
			switch (readstatus1) {
			case -2:
				printf("\nWARNING: extra newline at end of file 1\n");
				returncode = 7;
				break;
			case -4:
				printf("\nERROR: out of sync near line %i file 1\n", seqcount * 4 +1);
				printf("INFO:  could be caused by too long seq(>%i) or seqname(>%i)\n", SEQ_BUFFER_SIZE-2, NAME_BUFFER_SIZE-2);
				returncode = 8;
				break;
			}
			
			// close files
			closeFiles1(FILE_MAX, pSeqFile1, pOutFiles);
		}
	}
	
	if (taboutput == 0)
		printf(" Done\n\n");
	
	// write stats
	unsigned total = 0;
	for (i = 0; i <= TRIM_MAX; i++)
		total += counters[i];

	double dTotal = total / 100.0;
	
	if (taboutput == 0)
	{
		printf("Totals:\n");
		printf("* Reads:                  %u\n", total);
		if (paired==1)
		{
			printf("+ Trimmed (pass):         %u (%.2f%%, %u pairs)\n", counters[TRIM_OK], counters[TRIM_OK] / dTotal, counters[TRIM_OK] / 2);
			printf("+ Singleton (pass):       %u (%.2f%%)\n", counters[TRIM_SING], counters[TRIM_SING] / dTotal);
		}
		else
			printf("+ Trimmed (pass):         %u (%.2f%%)\n", counters[TRIM_OK], counters[TRIM_OK] / dTotal);
		printf("- Chastity (fail):        %u (%.2f%%)\n", counters[TRIM_CHAST], counters[TRIM_CHAST] / dTotal);
		printf("- Length (fail):          %u (%.2f%%)\n", counters[TRIM_LEN], counters[TRIM_LEN] / dTotal);
		printf("- Average Quality (fail): %u (%.2f%%)\n", counters[TRIM_AVGQU], counters[TRIM_AVGQU] / dTotal);
		printf("- N base count (fail):    %u (%.2f%%)\n", counters[TRIM_N], counters[TRIM_N] / dTotal);
//		printf("- Untrimmed (badness):    %u (%.2f%%)\n", counters[TRIM_UNTRIM], counters[TRIM_UNTRIM] / dTotal);
	}
	else
	{
		if (paired == 1)
		{
			printf("Source File1\tSource File2\tReads\tTrimmed\tPairs\tSingletons\tChastity\tLength\tAverage Quality\n");
			printf("%s\t%s\t%i\t%i\t%i\t%i\t%i\t%i\t%i\n",file1,file2,total,counters[TRIM_OK],counters[TRIM_OK] / 2,counters[TRIM_SING],counters[TRIM_CHAST],counters[TRIM_LEN],counters[TRIM_AVGQU]);
		}
		else
		{
			printf("Source File\tReads\tTrimmed\tChastity\tLength\tAverage Quality\tN base count\n");
			printf("%s\t%i\t%i\t%i\t%i\t%i\t%i\n",file1,total,counters[TRIM_OK],counters[TRIM_CHAST],counters[TRIM_LEN],counters[TRIM_AVGQU],counters[TRIM_N]);
		}
	}
	return returncode;
}



int main(int argc, char ** argv) {
	
	// parameter defines
	char ** filenames = NULL;
	short filecount = 0;
	unsigned short minquality = 15; 
	unsigned short minavgquality = 20;
	unsigned short minlength = 50;
	unsigned short maxpoorbases = 3;
	short maxnbases = -1;
	short tailgcheck = 0;
	short nochastity = 0;
	short overridegz = 0;
	short taboutput = 0;
	unsigned short qualOffset = 33;
	unsigned short trimStart = 0;
	
	int exitcode = 0;
	
	// arg vars
#ifdef USE_ARGTABLE
	struct arg_int  *minAvgQuality 	= arg_int0("a", "min-avg-quality", 	NULL, 	"Minimum read average quality          [default: 20]");
	struct arg_int  *minLength 		= arg_int0("l", "min-length", 		NULL, 	"Minimum read length                   [default: 50]");
	struct arg_int  *maxNBases 		= arg_int0("N", "max-n-bases", 		NULL, 	"Maximum N bases included              [default: -1 (Any)]");
	struct arg_int  *offset 		= arg_int0("o", "offset", 			NULL, 	"Phred score + <int> == ASCII code     [default: 33]");
	struct arg_int  *maxPoorBases 	= arg_int0("p", "max-poor-bases", 	NULL, 	"Maximum poor quality bases included   [default:  3]");
	struct arg_int  *minQuality 	= arg_int0("q", "min-quality", 		NULL, 	"Minimum base cutoff quality           [default: 15]");
	struct arg_int  *compress	 	= arg_int0("z", "compress", 		NULL, 	"0 = match input, 1 = gzip, 2 = plain  [default:  0]");

	struct arg_lit  *tailg 			= arg_lit0("g", "tail-g", 					"Remove G bases from tail of reads (NextSeq)");
	struct arg_lit  *help 			= arg_lit0("h", "help", 					"Print this help message");
	struct arg_lit  *noChastity     = arg_lit0("n", "no-chastity", 				"Don't attempt Ilumina Chastity check");
	struct arg_lit  *start 			= arg_lit0("s", "trim-start",				"Trim start of sequences too");
	struct arg_lit  *tab 			= arg_lit0("t", "tab", 						"Display only statistics in tab format");
	struct arg_lit  *showCompress	= arg_lit0("Z", "show-compress",			"Report zlib usage (return code: 0=yes, 10=no)");
	
	struct arg_file *files 			= arg_filen(NULL,NULL,"<file>",1, 2, 		"Specify 1 or 2 input files.  (Two for paired reads)");
	struct arg_end  *end = arg_end(20);
	void* argtable[] = {minAvgQuality,minLength,maxNBases,offset,maxPoorBases,minQuality,compress,tailg,help,noChastity,start,tab,showCompress,files,end};
	//{minQuality,minAvgQuality,minLength,maxPoorBases,noChastity,help,start,tab,files,end};
	
	// other vars
	const char* progname = "quality-trim";
	int nerrors;
	
	// check memory
	if (arg_nullcheck(argtable) != 0) {
		/* NULL entries were detected, some allocations must have failed */
		printf("%s: insufficient memory\n", progname);
		arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
		return 1;
	}
	
	// set defaults
	minAvgQuality->ival[0] = minavgquality;
	minLength->ival[0] = minlength;
	maxNBases->ival[0] = maxnbases;
	offset->ival[0] = qualOffset;
	maxPoorBases->ival[0] = maxpoorbases;
	minQuality->ival[0] = minquality;
	compress->ival[0] = overridegz;
	
	// parse arguments
	nerrors = arg_parse(argc, argv, argtable);
	
	// help text
	if (help->count > 0) {
		printf("Usage: %s", progname);
		arg_print_syntax(stdout, argtable, "\n");
		printf("This program Quality trims a single or pair of FastQ sequence files.\n");
		arg_print_glossary(stdout, argtable, "  %-25s %s\n");
		arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

#ifdef USE_ZLIB_ANY
		printf("ZLIB (gzip): Supported\n\n");
#else
		printf("ZLIB (gzip): NOT Supported\n\n");
#endif
		printf("Version: %s\n\n", VERSION);
		
		return 0;
	}
	
	if (showCompress->count > 0) {

#ifdef USE_ZLIB_ANY
		printf("ZLIB (gzip): Supported\n\n");
		return 0;
#else
		printf("ZLIB (gzip): NOT Supported\n\n");
		return 10;
#endif
	}
	
	// option errors
	if (nerrors > 0) {
		arg_print_errors(stdout, end, progname);
		printf("Try '%s --help' for more information.\n", progname);
		arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
		return 2;
	}
	
	// brief help
	if (argc == 1) {
		printf("Try '%s --help' for more information.\n", progname);
		arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
		return 0;
	}
	
	// copy arg params
	filenames = (char**)files->basename;
	filecount = files->count;
	minavgquality = minAvgQuality->ival[0];
	minlength = minLength->ival[0];
	maxnbases = maxNBases->ival[0];
	qualOffset = offset->ival[0];
	maxpoorbases = maxPoorBases->ival[0];
	overridegz = compress->ival[0];
	
	minquality = minQuality->ival[0];  
	nochastity = noChastity->count;
	tailgcheck = (tailg->count > 0);
	if (start->count > 0)
		trimStart = 1;
	if (tab->count > 0)
		taboutput = 1;
	
	// cleanup
	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
#else
	
	
// # ifdef USE_GETOPT
	int helpflag = 0;
	int index;
	int c;
	char * files[2];

	opterr = 0;

	while ((c = getopt (argc, argv, "q:a:l:p:z:o:N:ghnstZ")) != -1)
	switch (c)
	{
		case 'q':
			minquality = atoi(optarg);
		break;
		case 'a':
			minavgquality = atoi(optarg);
		break;
		case 'l':
			minlength = atoi(optarg);
		break;
		case 'p':
			maxpoorbases = atoi(optarg);
		break;
		case 'z':
			overridegz = atoi(optarg);
		break;
		case 'o':
			qualOffset = atoi(optarg);
		break;
		case 'N':
			maxnbases = atoi(optarg);
		break;
		case 'g':
			tailgcheck = 1;
		break;
		case 'h':
			helpflag = 1;
		break;
		case 'n':
			nochastity = 1;
		break;
		case 's':
			trimStart = 1;
		break;
		case 't':
			taboutput = 1;
		break;
		case '?':
			if (optopt == 'q' || optopt == 'a' || optopt == 'l' || optopt == 'p' || optopt == 'z')
				fprintf (stderr, "Option -%c requires an argument.\n", optopt);
			else
				fprintf (stderr, "Unknown option `-%c'.\n", optopt);
		return 1;
		case 'Z':
#ifdef USE_ZLIB_ANY
		printf("ZLIB (gzip): Supported\n\n");
		return 0;
#else
		printf("ZLIB (gzip): NOT Supported\n\n");
		return 10;
#endif
		default:
			return 3;
		break;
	}

	// get the file names
	c = 0;
	for (index = optind; index < argc && c <= 2; index++, c++)
	{
		files[c] = argv[index];
		filecount++;
	}
	filenames=files;
	
	// print help
	if (helpflag == 1)
	{
		printf("\nquality-trim [-hnstZ] [-a <int>] [-l <int>] [-N <int>] [-o <int>] [-p <int>]\n");
		printf("             [-q <int>] [-z [0-2]] <file1> [<file2>]\n\n");
		printf("-a <int>    Minimum read average quality          [default: 20]\n");
		printf("-l <int>    Minimum read length                   [default: 50]\n");
		printf("-N <int>    Maximum N bases included              [default: -1 (Any)]\n");
		printf("-o <int>    Phred score + <int> == ASCII code     [default: 33]\n");
		printf("-p <int>    Maximum poor quality bases included   [default:  3]\n");
		printf("-q <int>    Minimum base cutoff quality           [default: 15]\n");
		printf("-z <int>    0 = match input, 1 = gzip, 2 = plain  [default:  0]\n");
		printf("-g          Remove G bases from tail of reads (NextSeq)\n");
		printf("-h          Print this help message\n");
		printf("-n          Don't attempt Ilumina Chastity check\n");
		printf("-s          Trim start of sequences too\n");
		printf("-t          Display only statistics in tab format\n");
		printf("-Z          Report zlib usage (return code: 0=yes, 10=no)\n\n");

#ifdef USE_ZLIB
		printf("ZLIB (gzip): Supported\n\n");
#else
# ifdef USE_ZLIB_OLD
		printf("ZLIB (gzip): Supported\n\n");
# else
		printf("ZLIB (gzip): NOT Supported\n\n");
# endif
#endif
		printf("Version: %s\n\n", VERSION);
		
		return 0;
	}
	
#endif // end USE_ARGTABLES

	if (minavgquality == 0)
		minavgquality = minquality;
	
	// do the actual processing
	if (filecount > 0 && filecount <= 2)
		exitcode = qualityTrim(filenames, filecount, minquality, minavgquality, minlength, maxpoorbases, tailgcheck, nochastity, overridegz, taboutput, qualOffset, trimStart, maxnbases);
	else
		printf("quality-trim expects either 1 or 2 files only!\n");
	
	
	
	return exitcode;
}




